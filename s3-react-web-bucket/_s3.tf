# 
# File: s3.tf
# Auth: Martin Burolla
# Date: 7/14/2022
# Desc:
# 

resource "aws_s3_bucket" "s3-react-bucket" {
  bucket        =  var.bucket_name
  force_destroy = true
}

resource "aws_s3_bucket_public_access_block" "example" {
  bucket = aws_s3_bucket.s3-react-bucket.id
  block_public_acls   = false
  block_public_policy = false
}

resource "aws_s3_bucket_website_configuration" "s3-react-bucket" {
  bucket = aws_s3_bucket.s3-react-bucket.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}

data "aws_s3_bucket" "selected" {
  bucket = var.bucket_name
  depends_on = [
    aws_s3_bucket.s3-react-bucket
  ]
}
