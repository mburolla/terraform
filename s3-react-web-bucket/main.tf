# 
# File: main.tf
# Auth: Martin Burolla
# Date: 7/14/2022
# Desc:
# 

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.22.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "null_resource" "deploy-code" {
    count = var.should_deploy ? 1 : 0
    provisioner "local-exec" {
        command = "aws s3 sync ${var.build_directory} s3://${var.bucket_name}"
    }
    depends_on = [
      aws_s3_bucket.s3-react-bucket
    ]
}