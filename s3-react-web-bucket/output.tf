# 
# File: output.tf
# Auth: Martin Burolla
# Date: 7/14/2022
# Desc:
# 

output "s3_url" {
    value = "http://${data.aws_s3_bucket.selected.website_endpoint}"
}
