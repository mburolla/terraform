# 
# File: variables.tf
# Auth: Martin Burolla
# Date: 7/14/2022
# Desc:
# 

variable "bucket_name" { 
    type = string
    description = "A globally unique bucket name that contains the React application."
}

variable "deploy_iam_user_arn" { 
    type = string
    description = "The IAM user that has access to deploy application code to the bucket."
}

variable "build_directory" { 
    type = string
    description = "The directory that contains the production ready version of the React application."
    default = "./build"
}

variable "should_deploy" { 
    type = bool
    description = "Indicates if Terraform should use the AWS CLI to sync the contents of build_directory to S3."
    default = false
}
