# S3-React-Web-Bucket
Terraform module that provisions an AWS S3 bucket for a React web application.  Optionally can deploy code to the S3 bucket.

# Variables
#### Inputs
|Name|Description|Type|Example|Required|
|----|-----------|----|-------|--------|
|bucket_name|A globally unique bucket name that contains the React application|string|my-react-bucket|Yes|
|deploy_iam_user_arn|The IAM user that has access to deploy code to the bucket|string|arn:aws:iam::ACCOUNT:user/USER_NAME|Yes|
|build_directory|The directory that contains the production ready version of the React application.|string|./build|No|
|should_deploy|Indicates if Terraform should use the AWS CLI to sync the contents of `build_directory` to S3.|bool|true|No|

#### Outputs
|Name|Description|Type|Description|
|----|-----------|----|-----------|
|s3_url|string|http://siux-my-react-bucket.s3-website-us-east-1.amazonaws.com|The HTTP url associated with the React bucket|

# Provisioned Resources
- AWS S3 Bucket
  - Bucket Policy

# Usage

### Provision and Deploy
`main.tf`
```
variable "bucket_name" {}
variable "should_deploy" {}
variable "build_directory" {}
variable "deploy_iam_user_arn" {}

module "s3-react-web-bucket" {
    source = "./s3-react-web-bucket"
    bucket_name = var.bucket_name
    deploy_iam_user_arn = var.deploy_iam_user_arn
    should_deploy = var.should_deploy
    build_directory = var.build_directory
}

output "s3_url" {
    value = "${module.s3-react-web-bucket.s3_url}"
}
```

# Notes
Deployment only happens when the bucket is created for the FIRST time.
