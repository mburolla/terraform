# 
# File: bucket-policy.tf
# Auth: Martin Burolla
# Date: 7/14/2022
# Desc:
# 

resource "aws_s3_bucket_policy" "allow_web_access" {
  bucket = aws_s3_bucket.s3-react-bucket.id
  policy = data.aws_iam_policy_document.web-policy.json
}

data "aws_iam_policy_document" "web-policy" {
  statement {
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:GetObject",
      "s3:ListBucket",
    ]
    resources = [
      aws_s3_bucket.s3-react-bucket.arn,
      "${aws_s3_bucket.s3-react-bucket.arn}/*",
    ]
  }
  statement {
    principals {
      type        = "AWS"
      identifiers = [var.deploy_iam_user_arn]
    }
    actions = ["*"]
    resources = [
      aws_s3_bucket.s3-react-bucket.arn,
      "${aws_s3_bucket.s3-react-bucket.arn}/*",
    ]
  }
}
