#
# Buckets
#

resource "aws_s3_bucket" "s3-marty-bucket" {
  bucket        = "s3-${terraform.workspace}-marty-bucket-name"
  force_destroy = true
}

resource "aws_s3_bucket_policy" "s3-marty-bucket-policy" {
  bucket = aws_s3_bucket.s3-marty-bucket.id
  policy = data.aws_iam_policy_document.allow_access_from_another_account.json
}

data "aws_iam_policy_document" "allow_access_from_another_account" {
  statement {
    principals {
      type        = "AWS"
      identifiers = ["807758713182"]
    }

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
    ]

    resources = [
      aws_s3_bucket.s3-marty-bucket.arn,
      "${aws_s3_bucket.s3-marty-bucket.arn}/*",
    ]
  }
}
