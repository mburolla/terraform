terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.22.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

#########################################################################
# Variables
#########################################################################

locals {
  deEnv = terraform.workspace == "prod" ? "" : terraform.workspace
}

variable "the-env" {
  default = ""
}

variable "env" {
  type        = string
  default     = "dev"
  description = "The environment to deploy to"
}

variable "content" {}

output "pet-name" {
  value = random_pet.my-pet.id
}

output "vpc-id" {
  value = data.aws_vpc.default_vpc.id
}

output "test" {
  value = random_string.my-string.result
}

output "test2" {
  # value =  terraform.workspace == "prod" ? "" : terraform.workspace
  value = local.deEnv
}

output "rds" {
    value = data.aws_db_instance.database.engine
}

output "test4" {
    value = local_file.pet.content
}

resource "random_uuid" "test" {}

resource "random_string" "my-string" {
  length           = 16
  special          = true
  override_special = "/@£$"
}

#########################################################################
# Data: Existing resource in AWS
#########################################################################

data "aws_vpc" "default_vpc" {
  default = true
}

data "aws_db_instance" "database" {
    db_instance_identifier =  "database-2"
}

#########################################################################
# Resources
#########################################################################

resource "local_file" "pet" {
  filename = "pets.txt"
  # content = var.content
  content = "My favorite pet is: ${random_pet.my-pet.id}"

  depends_on = [
    random_pet.my-pet
  ]
}

resource "random_pet" "my-pet" {
  prefix    = "Mr."
  separator = "."
  length    = "1"
}

resource "aws_budgets_budget" "test-budget" {
  name              = "budget-${terraform.workspace}-monthly"
  budget_type       = "COST"
  limit_amount      = "700.0"
  limit_unit        = "USD"
  time_unit         = "MONTHLY"
  time_period_start = "2020-09-01_00:01"
}


#########################################################################
# Roles
#########################################################################

resource "aws_iam_role" "tf-test-role" {
  name = "iam-role-${terraform.workspace}-test-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

#########################################################################
# Policies
#########################################################################

resource "aws_iam_policy" "tf-policy-doc" {
  name        = "iam-policy-${terraform.workspace}-test-policy"
  path        = "/"
  description = "My test policy"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "ec2:Describe*",
        ]
        Resource = "*"
      },
    ]
  })
}

