# Terraform
Reference repo to get started with Terraform.  This repo contains a Terraform module called `s3-react-web-bucket` that provisions a bucket so that a React web application can run in it.  Read about it [here](./s3-react-web-bucket/readme.md).

# Prerequisites
- Install [Terraform](https://www.terraform.io/downloads.html)
- Install [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)

# Getting Started
- Clone this repo
- NOTE: The variables in `terraform.auto.tfvars` only exist to assist in the development of terraform

# Deploy
- Update variables at beginning of`deploy.bat` script
- Execute: `deploy.bat`

# Links
[S3](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket)

# Common Terraform Commands
- `terraform init`
- `terraform fmt`
- `terraform validate`
- `terraform plan`
- `terraform apply -auto-approve`
- `terraform apply -var "env=dev"`
- `terraform workspace list`
- `terraform workspace new`
- `terraform destory`
- `terraform output`

# Notes
- Data blocks:
  - Reference an existing resource in AWS
  - Reference local data (e.g. policy docs)
- Windows Terraform alias: Powershell: `New-Alias -Name "tf" -Value "terraform"`

# Terraform Variable Types
- String
- Number
- Boolean
- Any (by default if not specified)
- List
- Map
- Object
- Tuple
- Set

### Examples
- list(string): Strongly typed list of strings

# Meta Arguments
- depends_on: For specifying hidden dependencies
- count: For creating multiple resource instances according to a count
- for_each: To create multiple instances according to a map, or set of strings
- provider: For selecting a non-default provider configuration
- lifecycle: For lifecycle customizations
- provisioner: For taking extra actions after resource creation
