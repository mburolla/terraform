# 
# File: main.tf
# Auth: Martin Burolla
# Date: 7/14/2022
# Desc:
# 

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.22.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "bucket_name" {}
variable "deploy_iam_user_arn" {}
variable "build_directory" {}
variable "should_deploy" {}

module "s3-react-web-bucket" {
    source = "./s3-react-web-bucket"
    bucket_name = var.bucket_name
    deploy_iam_user_arn = var.deploy_iam_user_arn
    should_deploy = var.should_deploy
    build_directory = var.build_directory
}

output "s3_url" {
    value = "${module.s3-react-web-bucket.s3_url}"
}
