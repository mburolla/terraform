# Terraform Library
A collection of Terraform modules to help with the following scenarios:

|AWS Use case                                                                                |Module                                                 |
|--------------------------------------------------------------------------------------------|-------------------------------------------------------|
|Provision and optionally deploy React application to S3 bucket                              |[s3-react-web-bucket](./s3-react-web-bucket/readme.md) |
|Provision and optionally deploy React application to S3 bucket & CloudFront                 |[s3-react-web-bucket](./s3-react-web-bucket/readme.md) |
|Provision and optionally deploy React application to S3 bucket & CloudFront & Domain        |[s3-react-web-bucket](./s3-react-web-bucket/readme.md) |
|Provision and optionally deploy single Express API server                                   |TBD|
|Provision and optionally deploy multiple Express API servers & load balancer                |TBD|

# Approach
- Terraform workspaces are not used
- Each environment has a file that contains all the variables for that environment
- Modules do not call other modules
- Module structure:
  - `main.tf`
  - `output.tf`
  - `variables.tf` 
  - `readme.md`
  - `_*.tf` (supporting IaC stacks)

# Commands
- `tf apply -var-file ./envs/dev.tfvars -auto-approve`
- `tf destroy -var-file ./envs/dev.tfvars -auto-approve`
